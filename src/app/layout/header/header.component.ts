import { Component, OnInit, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Router } from '@angular/router';
// @Directive({ selector: '[trackScroll]' })

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  status = false;
  default = true;
  showHeader = false;
  constructor(public layoutService: LayoutService, private router: Router) { }

  ngOnInit(): void {
    window.scrollTo({top:0})
    this.layoutService.overlay_closed.subscribe((res: any)=> {
      if(res) {
        this.showHeader = res
      }
    });

    window.addEventListener('scroll', this.scrollEvent, true);
  }
  showMenu(id) {

    this.layoutService.isOpened.next(true);
    this.layoutService.selected_genre.next(id);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scrollEvent, true);
  }

  scrollEvent = (event: any): void => {
    const number = event.srcElement.scrollTop;
    console.log(number)
    if(number > 90) {
      this.default = false;
    } else {
      this.default = true;
    }
  }
  to_home() {
    this.router.navigate(['/home'])
  }
  to_login(){
    this.router.navigate(['/auth/login'])
  }
}
