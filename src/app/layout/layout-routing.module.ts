import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';


const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      },
      {
        path: 'home',
        loadChildren: ()=> import('../features/dashboard/dashboard.module').then(m=> m.DashboardModule),
        data: { state: 'home'} 
      },
      {
        path: 'auth',
        loadChildren: ()=> import('../features/auth/auth.module').then(m=> m.AuthModule),
        data: { state: 'auth'} 
      }
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
