import { Component, OnInit, Input} from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  // animations: [slideInAnimation]
})
export class MainLayoutComponent implements OnInit {
  _opened: boolean;
  subscriptions: Subscription[] = [];
  showLanding = false;
  sidebarState;
  showFiller;
  constructor(public layoutService: LayoutService,private router: Router) { 
  }

  ngOnInit(): void {
    this.layoutService.filler.next(false);

    // setTimeout(() => {
    //   this.showFiller = true
    //   // this.showLanding = false;
    //   // this.layoutService.overlay_closed.next(true);
    // }, 5000);
  }
  // alphaClick() {    
  //   this.layoutService.isOpened.next({type: 0, status: true})
  //   // this.showSideNav = !this.showSideNav  
  // }
  ngOnDestroy() {
    if (this.subscriptions.length) {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
  }

  // getRouteAnimation(outlet: RouterOutlet) {
  //   return outlet && outlet.activatedRouteData && outlet.activatedRouteData.state;
  // }
}
