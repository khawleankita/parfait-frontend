import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  selection;

  constructor(private router: Router, public layoutService: LayoutService) { }

  ngOnInit(): void {
    this.layoutService.selected_genre.subscribe((data) => {
      if(data) {
        this.selection = data;
      } else {
        this.selection = 1;
      }
    });
  }
  to_list() {
    this.layoutService.isOpened.next(false);
    // debugger
    this.layoutService.filler.next(true);
    this.router.navigate(['/home/list'])
  }
  showMenu(id) {
    // this.layoutService.isOpened.next(true);
    this.layoutService.selected_genre.next(id);
  }
  closeSidebar() {
    this.layoutService.isOpened.next(false);
  }
}
