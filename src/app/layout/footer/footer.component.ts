import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  offerForm: FormGroup;
  constructor() { }

  ngOnInit(): void {

    this.offerForm = new FormGroup(
      {email : new FormControl() 
      });
  }
  submit() {
    alert('submitted')
  }
}
