
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
// import { AuthService } from './auth/auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DomainInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('intercepted http request');
    request = request.clone({
      setHeaders: {
        'domain-url': 'http://localhost:5001'
        // dev
        // 'domain-url': 'https://newux.fxsocio.com'
        // stage
        // 'domain-url': 'https://stage-newux.fxsocio.com'
        // stage-oz
        // 'domain-url': 'https://stage-oz.fxsocio.com'
        // ntt1
        //  'domain-url': 'https://ntt1.fxsocio.com'
        // strech
        // 'domain-url': 'https://stretch.tradesocio.com'
        // nxcloud
        // 'domain-url': 'https://nxcloud.tradesocio.com'

      }
    });
    return next.handle(request);
  }
}
