import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, Routes, NavigationEnd, RouterOutlet } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { slideInAnimation } from './core/animation';
import { trigger, transition, query, style, animate } from '@angular/animations';
import { filter, take } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation]
})
export class AppComponent  implements OnInit {
  title = 'parfait-frontend';

  constructor(private router: Router,
    private viewPortScroller: ViewportScroller)
  {
  }
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      var scrollToTop = window.setInterval(function () {
        // debugger
          var pos = window.pageYOffset;
          if (pos > 0) {
              window.scrollTo(0, 0); // how far to scroll on each step
          } else {
              window.clearInterval(scrollToTop);
          }
      }, 0); // how fast to scroll (this equals roughly 60 fps)

    });

  }
  // getRouteAnimation(outlet: RouterOutlet) {
  //   console.log(outlet.activatedRouteData.state);
  //   return outlet && outlet.activatedRouteData && outlet.activatedRouteData.state;
  // }

}
