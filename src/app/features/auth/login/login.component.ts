import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  subscriptions: Subscription[] = [];
  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        email : new FormControl() ,
        password : new FormControl() ,
      });
  }
  to_signup() {
    this.router.navigate(['/auth/sign-up'])
  }
  return_home() {
    this.router.navigate(['/home'])
  }

  submit() {
    this.subscriptions.push(
      this.authService.login(this.loginForm.value).subscribe((res: any)=> {
        if(res && res.status) {
          this.router.navigate(['/home']);
          alert('logged in successfully')

        } else {
          alert('Could not login')
        }
      })
    )
  }
}
