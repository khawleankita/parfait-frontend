import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  constructor(private router: Router) { }

  ngOnInit(): void {

    this.signUpForm = new FormGroup(
      {
        first_name : new FormControl() ,
        last_name : new FormControl() ,
        email : new FormControl() ,
        password : new FormControl() ,
        re_password: new FormControl() ,
      });
  }

  to_login() {
    this.router.navigate(['/auth/login'])
  }
  return_home() {
    this.router.navigate(['/home'])
  }

}
