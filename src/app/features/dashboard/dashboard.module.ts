import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainDashboardComponent } from './pages/main-dashboard/main-dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductComponent } from './pages/product/product.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselComponent } from './pages/main-dashboard/carousel/carousel.component';
// import { LayoutModule } from 'src/app/layout/layout.module';

import { CarouselModule } from 'ngx-owl-carousel-o';
@NgModule({
  declarations: [
    MainDashboardComponent,
    ProductListComponent,
    ProductComponent,
    CarouselComponent,
    
  ],
  imports: [
    CommonModule,
    // LayoutModule,
    DashboardRoutingModule,
    NgbModule,
    CarouselModule,
  ]
})
export class DashboardModule { }
