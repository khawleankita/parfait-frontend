import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './pages/main-dashboard/main-dashboard.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductComponent } from './pages/product/product.component';

const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    data: { state: 'main-dashboard'} 
  },
  {
    path: 'list',
    component: ProductListComponent,
    data: { state: 'list'} 
  },
  {
    path: 'product',
    component: ProductComponent,
    data: { state: 'product'} 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
