import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    nav: false,
    autoplay: true
  }

  slidesStore = [
    {src: "../../../../../../assets/profiles/sample_1.jpg", id:1, title: "test1", name: "Mihael Keehl", comment: "An experience of an lifetime." }, 
    {src:"../../../../../../assets/profiles/sample_2.jpg", id:2, title: "test1",  name: "Terry Holland", comment: "A new take on men's fashion."},
    {src: "../../../../../../assets/profiles/sample_3.jpg" , id:3,title: "test1",  name: "Laura Vitale", comment: "Just the right amount of simplicity with a jazz, I love it."},
    {src: "../../../../../../assets/profiles/sample_1.jpg", id:1, title: "test1", name: "Mihael Keehl", comment: "An experience of an lifetime." }, 
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
