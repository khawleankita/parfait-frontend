import { Component, OnInit, OnDestroy } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  isLoading = false;
  constructor(public layoutService: LayoutService) { }

  ngOnInit() {
    // this.layoutService.filler.subscribe(res=> {
    //   console.log(res);
      
    // })
    this.layoutService.filler.next(true);
    // this.isLoading = true
    setTimeout(() => {
      // this.isLoading = false
      debugger
      this.layoutService.filler.next(false);
      // this.showLanding = false;
      // this.layoutService.overlay_closed.next(true);
    }, 2000);
  }
  ngOnDestroy() {
    // this.layoutService.filler.unsubscribe();
  }
}
