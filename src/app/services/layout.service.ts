import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  isOpened: BehaviorSubject<any> = new BehaviorSubject(false);
  selected_genre: BehaviorSubject<any> = new BehaviorSubject(null);
  overlay_closed: BehaviorSubject<Boolean> = new BehaviorSubject(false);
  filler: BehaviorSubject<any> = new BehaviorSubject(false);
  constructor() { }
}
